DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS friends;
DROP TABLE IF EXISTS game;

/*USERDATA*/
CREATE TABLE users (
	username TEXT NOT NULL,
	password TEXT NOT NULL,
	CONSTRAINT users_pk PRIMARY KEY (username)
);

/*USER FRIEND LIST AND FRIEND REQUEST STATUS*/
/*INFO:

status = 0; friend request pending
status = 1; friend request accepted

*/
CREATE TABLE friends (
	friend1username TEXT NOT NULL,
	friend2username TEXT NOT NULL,
	status INTEGER NOT NULL,
	CONSTRAINT friends_pk PRIMARY KEY (friend1username, friend2username)
);


/*GAME*/
/*INFO:

boardstate = 64 character string, reads horizontally left to right, i.e. first 8 are row one, second 8 are row two.
0 = blank
k/K = white/black king
q/Q = white/black queen
s/S = white/black bishop
r/R = white/black rook
n/N = white/black (k)night
p/P = white/black pawn (has performed first move)
u/U = white/black unmoved pawn  (has not performed first move)

current = 1; player1 2; player2
completed = 0; no 1; yes
winner = 0; not-complete/tie 1; player1 2; player 2
*/
CREATE TABLE game (
	gameid INTEGER PRIMARY KEY,
	player1username TEXT NOT NULL,
	player2username TEXT NOT NULL,
	boardstate TEXT NOT NULL,
    current INTEGER NOT NULL,
    completed INTEGER NOT NULL,
    winner INTEGER NOT NULL
);
