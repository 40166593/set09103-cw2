$(document).ready(function(){
  var gameidclasses = $('.gameid').attr('class').split(' ');
  var gameid = gameidclasses[1];
  if($(".yourturn").length){
	var playerColourClasses = $('.yourturn').attr('class').split(' ');
    var playerColour = playerColourClasses[1];
	if($('.'+playerColour+'.k').length) {
      $('.piece').click(function(){
        if($(this).hasClass("selected")){
          //alert("aaaa");
        }
        else
        {
          $(".selected").removeClass("selected");
          $(".current").removeClass("current");
          $(this).addClass("current");
          var coords = this.classList;
          var xCoord = coords[0];
          var yCoord = coords[1];
          var player = coords[5];
          //alert("1");
    
          if($(this).hasClass("u " + playerColour)){
            moveupawn(xCoord, yCoord, player);
          }
          else if($(this).hasClass("p " + playerColour)){
            movepawn(xCoord, yCoord, player);
          }
          else if($(this).hasClass("r " + playerColour)){
            moverook(xCoord, yCoord, player);
          }
	  	  else if($(this).hasClass("s " + playerColour)){
		    movebishop(xCoord, yCoord, player);
		  }
		  else if($(this).hasClass("q " + playerColour)){
		    moverook(xCoord, yCoord, player);
		    movebishop(xCoord, yCoord, player);
		  }
		  else if($(this).hasClass("k " + playerColour)){
		    moveking(xCoord, yCoord, player);
		  }
		  else if($(this).hasClass("n " + playerColour)){
		    moveknight(xCoord, yCoord, player);
		  }
        }

        $('.selected').click(function(){
          //alert("2");
          var current = $('.current').attr('class');
          var selected = $(this).attr('class');
          //alert(current + '\n' + selected);
          $.post("/updategame/" + gameid,
          {
            fromLocation: current,
            toLocation: selected
          });
          var myVar = setInterval(reload, 1000);
          function reload() {
            location.reload()
          }
        });
      });
	}
	else
	{
		$.post("/completegame/" + gameid,
		{
		  player : playerColour	
		});
		function reload() {
            location.reload()
        }
	}
  }
  else{
    var currentPlayerClass = $(".player").attr("class").split(' ');
    var currentPlayer = currentPlayerClass[1];
    
    var myVar = setInterval(function(){ myTimer(currentPlayer, gameid) }, 1000);
  }
});

function myTimer(currentPlayer, gameid) {
    $.post("/getgame/" + gameid, { current : currentPlayer }, function(data, status){
        console.log(data);
        if(data == "True"){
          location.reload();
        }
    });
}

function moveupawn(x, y, player){
  var increment = 0;
  var opponent = "";

  if(player=="pwhite"){
    increment = 1;
    opponent = "pblack";
  }
  if(player=="pblack"){
    increment = -1;
    opponent = "pwhite";
  }

  var newY = ((parseInt(y, 36)+increment).toString(36)).replace(/0/g,'a');

  var newX1 = ((parseInt(x, 36)+1).toString(36)).replace(/0/g,'a');
  var newX2 = ((parseInt(x, 36)-1).toString(36)).replace(/0/g,'a');

  if($('.'+newX1+'.'+newY).hasClass(opponent)){
    $('.'+newX1+'.'+newY).addClass("selected");
  }

  if($('.'+newX2+'.'+newY).hasClass(opponent)){
    $('.'+newX2+'.'+newY).addClass("selected");
  }

  if($('.'+x+'.'+newY).hasClass("piece")){
    return;
  }
  $('.'+x+'.'+newY).addClass("selected");

  newY = ((parseInt(newY, 36)+increment).toString(36)).replace(/0/g,'a');
  if($('.'+x+'.'+newY).hasClass("piece")){
    return;
  }
  $('.'+x+'.'+newY).addClass("selected");
}

function movepawn(x, y, player){
  var increment = 0;
  var opponent = "";

  if(player=="pwhite"){
    increment = 1;
    opponent = "pblack";
  }
  if(player=="pblack"){
    increment = -1;
    opponent = "pwhite";
  }

  var newY = ((parseInt(y, 36)+increment).toString(36)).replace(/0/g,'a');

  var newX1 = ((parseInt(x, 36)+1).toString(36)).replace(/0/g,'a');
  var newX2 = ((parseInt(x, 36)-1).toString(36)).replace(/0/g,'a');

  if($('.'+newX1+'.'+newY).hasClass(opponent)){
    $('.'+newX1+'.'+newY).addClass("selected");
  }

  if($('.'+newX2+'.'+newY).hasClass(opponent)){
    $('.'+newX2+'.'+newY).addClass("selected");
  }

  if($('.'+x+'.'+newY).hasClass("piece")){
    return;
  }
  $('.'+x+'.'+newY).addClass("selected");
}

function moverook(x, y, player){

  var opponent = "";

  if(player=="pwhite"){
    opponent = "pblack";
  }
  if(player=="pblack"){
    opponent = "pwhite";
  }

  var increment = 1;
  var newY = ((parseInt(y, 36)+increment).toString(36)).replace(/0/g,'a');
  var flag = true;

  while(flag){
    if(newY == '9'){
      flag = false;
    }
    else if($('.'+x+'.'+newY).hasClass(opponent)){
      $('.'+x+'.'+newY).addClass("selected");
      flag = false;
    }
    else if($('.'+x+'.'+newY).hasClass(player)){
      flag = false;
    }
    else{
      $('.'+x+'.'+newY).addClass("selected");
    }
    newY = ((parseInt(newY, 36)+increment).toString(36)).replace(/0/g,'a');
  }

  increment = -1;
  newY = ((parseInt(y, 36)+increment).toString(36)).replace(/0/g,'a');
  flag = true;

  while(flag){
    if(newY == 'a'){
      flag = false;
    }
    else if($('.'+x+'.'+newY).hasClass(opponent)){
      $('.'+x+'.'+newY).addClass("selected");
      flag = false;
    }
    else if($('.'+x+'.'+newY).hasClass(player)){
      flag = false;
    }
    else{
      $('.'+x+'.'+newY).addClass("selected");
    }
    newY = ((parseInt(newY, 36)+increment).toString(36)).replace(/0/g,'a');
  }

  increment = 1;
  newX = ((parseInt(x, 36)+increment).toString(36)).replace(/0/g,'a');
  flag = true;

  while(flag){
    if(newX == 'i'){
      flag = false;
    }
    else if($('.'+newX+'.'+y).hasClass(opponent)){
      $('.'+newX+'.'+y).addClass("selected");
      flag = false;
    }
    else if($('.'+newX+'.'+y).hasClass(player)){
      flag = false;
    }
    else{
      $('.'+newX+'.'+y).addClass("selected");
    }
    newX = ((parseInt(newX, 36)+increment).toString(36)).replace(/0/g,'a');
  }

  increment = -1;
  newX = ((parseInt(x, 36)+increment).toString(36)).replace(/0/g,'a');
  flag = true;

  while(flag){
    if(newX == '9'){
      flag = false;
    }
    else if($('.'+newX+'.'+y).hasClass(opponent)){
      $('.'+newX+'.'+y).addClass("selected");
      flag = false;
    }
    else if($('.'+newX+'.'+y).hasClass(player)){
      flag = false;
    }
    else{
      $('.'+newX+'.'+y).addClass("selected");
    }
    newX = ((parseInt(newX, 36)+increment).toString(36)).replace(/0/g,'a');
  }
}

function movebishop(x, y, player){
  var opponent = "";

  if(player=="pwhite"){
    opponent = "pblack";
  }
  if(player=="pblack"){
    opponent = "pwhite";
  }

  var xIncrement = 1;
  var yIncrement = 1;
  var newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  var newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  var flag = true;

  while(flag){
    if(newX == 'i' || newY == '9'){
      flag = false;
    }
    else if($('.'+newX+'.'+newY).hasClass(opponent)){
      $('.'+newX+'.'+newY).addClass("selected");
      flag = false;
    }
    else if($('.'+newX+'.'+newY).hasClass(player)){
      flag = false;
    }
    else{
      $('.'+newX+'.'+newY).addClass("selected");
    }
	newX = ((parseInt(newX, 36)+xIncrement).toString(36)).replace(/0/g,'a');
    newY = ((parseInt(newY, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  }

  yIncrement = -1;
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  flag = true;

  while(flag){
    if(newX == 'i' || newY == 'a'){
      flag = false;
    }
    else if($('.'+newX+'.'+newY).hasClass(opponent)){
      $('.'+newX+'.'+newY).addClass("selected");
      flag = false;
    }
    else if($('.'+newX+'.'+newY).hasClass(player)){
      flag = false;
    }
    else{
      $('.'+newX+'.'+newY).addClass("selected");
    }
	newX = ((parseInt(newX, 36)+xIncrement).toString(36)).replace(/0/g,'a');
    newY = ((parseInt(newY, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  }

  xIncrement = -1;
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  flag = true;

  while(flag){
    if(newX == '9' || newY == 'a'){
      flag = false;
    }
    else if($('.'+newX+'.'+newY).hasClass(opponent)){
      $('.'+newX+'.'+newY).addClass("selected");
      flag = false;
    }
    else if($('.'+newX+'.'+newY).hasClass(player)){
      flag = false;
    }
    else{
      $('.'+newX+'.'+newY).addClass("selected");
    }
    newX = ((parseInt(newX, 36)+xIncrement).toString(36)).replace(/0/g,'a');
	newY = ((parseInt(newY, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  }

  yIncrement = 1;
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  flag = true;

  while(flag){
    if(newX == '9' || newY == '9'){
      flag = false;
    }
    else if($('.'+newX+'.'+newY).hasClass(opponent)){
      $('.'+newX+'.'+newY).addClass("selected");
      flag = false;
    }
    else if($('.'+newX+'.'+newY).hasClass(player)){
      flag = false;
    }
    else{
      $('.'+newX+'.'+newY).addClass("selected");
    }
    newX = ((parseInt(newX, 36)+xIncrement).toString(36)).replace(/0/g,'a');
	newY = ((parseInt(newY, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  }
}

function moveking(x, y, player){

  var opponent = "";
  var xIncrement = 0;
  var yIncrement = 0;
  var newX = 0;
  var newY = 0;

  if(player=="pwhite"){
    opponent = "pblack";
  }
  if(player=="pblack"){
    opponent = "pwhite";
  }
  
  xIncrement = 0;
  yIncrement = 1;
  
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  
  if(!$('.'+newX+'.'+newY).hasClass(player) && newY!='9'){
    $('.'+newX+'.'+newY).addClass("selected");
  }
  
  xIncrement = 1;
  yIncrement = 1;
  
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  
  if(!$('.'+newX+'.'+newY).hasClass(player) && (newX!='i' && newY!='9')){
    $('.'+newX+'.'+newY).addClass("selected");
  }
  
  xIncrement = 1;
  yIncrement = 0;
  
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  
  if(!$('.'+newX+'.'+newY).hasClass(player) && newX!='i'){
    $('.'+newX+'.'+newY).addClass("selected");
  }
  
  xIncrement = 1;
  yIncrement = -1;
  
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  
  if(!$('.'+newX+'.'+newY).hasClass(player) && (newX!='i' && newY!='a')){
    $('.'+newX+'.'+newY).addClass("selected");
  }
  
  xIncrement = 0;
  yIncrement = -1;
  
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  
  if(!$('.'+newX+'.'+newY).hasClass(player) && newY!='a'){
    $('.'+newX+'.'+newY).addClass("selected");
  }
  
  xIncrement = -1;
  yIncrement = -1;
  
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  
  if(!$('.'+newX+'.'+newY).hasClass(player) && (newX!='9' && newY!='a')){
    $('.'+newX+'.'+newY).addClass("selected");
  }
  
  xIncrement = -1;
  yIncrement = 0;
  
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  
  if(!$('.'+newX+'.'+newY).hasClass(player) && newX!='9'){
    $('.'+newX+'.'+newY).addClass("selected");
  }
  
  xIncrement = -1;
  yIncrement = 1;
  
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
  
  if(!$('.'+newX+'.'+newY).hasClass(player) && (newX!='9' && newY!='i')){
    $('.'+newX+'.'+newY).addClass("selected");
  }
}

function moveknight(x, y, player){
  var opponent = "";
  var xIncrement = 0;
  var yIncrement = 0;
  var newX = 0;
  var newY = 0;

  if(player=="pwhite"){
    opponent = "pblack";
  }
  if(player=="pblack"){
    opponent = "pwhite";
  }
  
  xIncrement = 0;
  yIncrement = 1;
  
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');

  if(newY != '9'){
    xIncrement = 1;
    newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
    if(newX != 'i'){
      xIncrement = 2;
      newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
      if(newX != 'i' && !$('.'+newX+'.'+newY).hasClass(player)){
        $('.'+newX+'.'+newY).addClass("selected");
      }
    }
    xIncrement = -1;
    newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
    if(newX != '9'){
      xIncrement = -2;
      newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
      if(newX != '9' && !$('.'+newX+'.'+newY).hasClass(player)){
        $('.'+newX+'.'+newY).addClass("selected");
      }
    }

    yIncrement = 2;
    newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
    if(newY != '9'){
      xIncrement = 1;
      newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
      if(newX != 'i' && !$('.'+newX+'.'+newY).hasClass(player)){
        $('.'+newX+'.'+newY).addClass("selected");
      }
      xIncrement = -1;
      newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
      if(newX != '9' && !$('.'+newX+'.'+newY).hasClass(player)){
        $('.'+newX+'.'+newY).addClass("selected");
      }
    }
  }

  xIncrement = 0;
  yIncrement = -1;
  
  newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
  newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');

  if(newY != 'a'){
    xIncrement = 1;
    newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
    if(newX != 'i'){
      xIncrement = 2;
      newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
      if(newX != 'i' && !$('.'+newX+'.'+newY).hasClass(player)){
        $('.'+newX+'.'+newY).addClass("selected");
      }
    }
    xIncrement = -1;
    newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
    if(newX != '9'){
      xIncrement = -2;
      newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
      if(newX != '9' && !$('.'+newX+'.'+newY).hasClass(player)){
        $('.'+newX+'.'+newY).addClass("selected");
      }
    }

    yIncrement = -2;
    newY = ((parseInt(y, 36)+yIncrement).toString(36)).replace(/0/g,'a');
    if(newY != 'a'){
      xIncrement = 1;
      newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
      if(newX != 'i' && !$('.'+newX+'.'+newY).hasClass(player)){
        $('.'+newX+'.'+newY).addClass("selected");
      }
      xIncrement = -1;
      newX = ((parseInt(x, 36)+xIncrement).toString(36)).replace(/0/g,'a');
      if(newX != '9' && !$('.'+newX+'.'+newY).hasClass(player)){
        $('.'+newX+'.'+newY).addClass("selected");
      }
    }
  }
}
