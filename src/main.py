from flask import Flask, g, render_template, session, url_for, redirect, request, escape
from shutil import copyfile
import sqlite3
import sys
import ConfigParser
import bcrypt
import os
import struct

app = Flask(__name__)
app.secret_key = 'A0Zr98j/3yXR~XHH!jmN]LWX/,?RT'
db_location = sys.path[0] + '/var/chess.db'
con_location = sys.path[0] + '/etc/defaults.cfg'

class Game:
  def __init__(self, id, opponent):
    self.id = id
    self.opponent = opponent

def init(app):
  config = ConfigParser.ConfigParser()
  try:
    config_location = con_location
    config.read(config_location)

    app.config['Debug'] = config.get("config", "debug")
    app.config['ip_address'] = config.get("config", "ip_address")
    app.config['port'] = config.get("config", "port")
    app.config['url'] = config.get("config", "url")
  except:
    print "Could not read configs from: ", config_location

def get_db():
  db = getattr(g, 'db', None)
  if db is None:
    db = sqlite3.connect(db_location)
    g.db = db
  return db

def init_db():
  with app.app_context():
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
      db.cursor().executescript(f.read())
    db.commit()

def check_auth(username, password):
  db = get_db()

  sql = "SELECT username, password FROM users WHERE username = \"" + username + "\";"

  db_pass = ''

  for row in db.cursor().execute(sql):
    db_pass = str(row[1])

  validpass = db_pass

  print "1: " + validpass
  print "2: " + bcrypt.hashpw(password.encode('utf-8'),validpass)


  if (validpass == bcrypt.hashpw(password.encode('utf-8'),validpass)):
    return True
  else:
    return False

def charToNum(s):
  return {
    'a' : '1',
    'b' : '2',
    'c' : '3',
    'd' : '4',
    'e' : '5',
    'f' : '6',
    'g' : '7',
    'h' : '8',
  }.get(s)

@app.route("/", methods=['GET','POST'])
def root():

  name = ''
  db = get_db()

  if request.method=='POST':
    if request.form['response']=='accept':
      sql = "UPDATE friends SET status = 1 WHERE friend1username = \"" + request.form['user'] + "\" AND friend2username = \"" + session['username'] + "\";"
    elif request.form['response']=='deny':
      sql = "DELETE FROM friends WHERE friend1username = \"" + request.form['user'] + "\" AND friend2username = \"" + session['username'] + "\";"
    db.cursor().execute(sql)
    db.commit()

  if 'username' in session:
    name = session['username']

    sql1 = "SELECT * FROM users u INNER JOIN friends f ON u.username = f.friend1username WHERE u.username = \"" + session['username'] + "\" AND status = 1;"
    sql2 = "SELECT * FROM users u INNER JOIN friends f ON u.username = f.friend2username WHERE u.username = \"" + session['username'] + "\" AND status = 1;"

    friends = []

    for row in db.cursor().execute(sql1):
      friends.append(str(row[3]))

    for row in db.cursor().execute(sql2):
      friends.append(str(row[2]))

    sqlpending = "SELECT * FROM users u INNER JOIN friends f ON u.username = f.friend1username WHERE f.friend2username = \"" + session['username'] + "\"AND status = 0;"

    pending = []

    for row in db.cursor().execute(sqlpending):
      pending.append(str(row[2]))

    games = []

    sqlgames1 = "SELECT * FROM game WHERE player1username = \"" + session['username'] + "\" AND completed = 0;"
    sqlgames2 = "SELECT * FROM game WHERE player2username = \"" + session['username'] + "\" AND completed = 0;"

    for row in db.cursor().execute(sqlgames1):
      games.append(Game(str(row[0]),str(row[2])))

    for row in db.cursor().execute(sqlgames2):
      games.append(Game(str(row[0]),str(row[1])))

    return render_template("index.html", username=name, pending=pending, friends=friends, games=games)

  return render_template("indexnolog.html", username="");

@app.route("/users/")
def users():
  name = ''
  if 'username' in session:
    name = session['username']

  sql = "SELECT * FROM users"
  listOfUsers = []

  db = get_db()
  for row in db.cursor().execute(sql):
    listOfUsers.append(str(row[0]))

  return render_template("users.html", users=listOfUsers, username=name)

@app.route("/register/", methods=['GET','POST'])
def register():
  name = ''
  if 'username' in session:
    name = session['username']

  if request.method == 'POST':
    if request.form['username'] != '' and request.form['password'] != '':
      user = request.form['username']
      pw = request.form['password']
	  
      db = get_db()
	  
      sqlusers = "SELECT * FROM users WHERE username = \"" + user + "\";"
	  
      alreadyuser = False
	  
      for row in db.cursor().execute(sqlusers):
        alreadyuser = True
		
      if alreadyuser:
        return render_template("register.html", username=name, message="Registration failed, name already in use.")

      sql = "INSERT INTO users VALUES(\"" + user + "\", \"" + bcrypt.hashpw(pw, bcrypt.gensalt()) + "\");"

      db.cursor().execute(sql)
      db.commit()
	  
      copyfile('static/resources/chessimages/blank.png', 'static/resources/userimages/' + request.form['username'] + '.png')

      return redirect("/login/", code=302)
    else:
      return render_template("register.html", username=name, message="Registration failed")
  else:
    return render_template("register.html", username=name)

@app.route("/login/", methods=['GET','POST'])
def login():
  name = ''
  if 'username' in session:
    name = session['username']

  if request.method == 'POST':
    if check_auth(request.form['username'],request.form['password']):
      session['username'] = request.form['username']
      return redirect(url_for('root'))
    else:
      return render_template("message.html", message="Login failed")
  return render_template("login.html", username=name)

@app.route("/logout/")
def logout():
  session.pop('username', None)
  return redirect(url_for('root'))

@app.route("/user/<profilename>", methods=['GET','POST'])
def userprofile(profilename):

  name = ''
  if 'username' in session:
    name = session['username']


  db = get_db()

  if request.method=='POST':
    if request.form['task']=='remove':
      sqltask = "DELETE FROM friends WHERE (friend1username = \"" + session['username'] + "\" AND friend2username = \"" + profilename + "\") OR (friend1username = \"" + profilename + "\" AND friend2username = \"" + session['username'] + "\");"
    elif request.form['task']=='add':
      sqltask = "INSERT INTO friends VALUES(\"" + session['username'] + "\",\"" + profilename + "\",0);"
    db.cursor().execute(sqltask)
    db.commit()
  sql = "SELECT username FROM users WHERE username = \"" + profilename + "\";"

  userprofile = ''
  for row in db.cursor().execute(sql):
    userprofile = str(row[0])

  if userprofile == '':
    return render_template("message.html", message="User profile not found")
	
  wins = 0
  losses = 0

  sqlgames1 = "SELECT * FROM game WHERE player1username = \"" + profilename + "\" AND completed = 1;"
  sqlgames2 = "SELECT * FROM game WHERE player2username = \"" + profilename + "\" AND completed = 1;"

  for row in db.cursor().execute(sqlgames1):
    if row[6] == 1:
	  wins = wins+1
    elif row[6] == 2:
	  losses = losses+1
  for row in db.cursor().execute(sqlgames2):
    if row[6] == 2:
	  wins = wins+1
    elif row[6] == 1:
	  losses = losses+1
	  
  sqlgames3 = "SELECT * FROM game WHERE player1username = \"" + profilename + "\" AND completed = 0;"
  sqlgames4 = "SELECT * FROM game WHERE player2username = \"" + profilename + "\" AND completed = 0;"
  
  games = []
  
  for row in db.cursor().execute(sqlgames3):
    games.append(Game(str(row[0]),str(row[2])))
  for row in db.cursor().execute(sqlgames4):
    games.append(Game(str(row[0]),str(row[1])))
	
  print games
  
  sql1 = "SELECT * FROM users u INNER JOIN friends f ON u.username = f.friend1username WHERE u.username = \"" + profilename + "\" AND status = 1;"
  sql2 = "SELECT * FROM users u INNER JOIN friends f ON u.username = f.friend2username WHERE u.username = \"" + profilename + "\" AND status = 1;"

  friends = []

  for row in db.cursor().execute(sql1):
    friends.append(str(row[3]))

  for row in db.cursor().execute(sql2):
    friends.append(str(row[2]))

  arefriends = False
  ispending = False
  notself = False

  if 'username' in session: 
    if session['username'] in friends:
      arefriends = True

    sqlpending = "SELECT * FROM users u INNER JOIN friends f ON u.username = f.friend2username WHERE u.username = \"" + profilename + "\" AND f.friend1username = \"" + session['username'] + "\"AND status = 0;"

    pending = []

    for row in db.cursor().execute(sqlpending):
      pending.append(str(row[2]))

    if len(pending) > 0:
      ispending=True

    if session['username'] != profilename:
      notself=True

  return render_template("user.html", username=name, user=userprofile, friends=friends, arefriends=arefriends, pending=ispending, notself=notself, wins=wins, losses=losses, games=games)

@app.route("/challenge/<opponent>")
def challenge(opponent):
  if 'username' not in session:
    return render_template("message.html", username='', message="Cannot create game if not logged in")
  else:
    if session['username'] == opponent:
      return render_template("message.html", username=session['username'], message="cannot start game against self")

    db = get_db()

    sql = "SELECT * FROM game WHERE completed = 0 AND(player1username = \"" + session['username'] + "\" AND player2username = \"" + opponent + "\") OR (player1username = \"" + opponent + "\" AND player2username = \"" + session['username'] + "\");"

    alreadyhavegame = False

    for row in db.cursor().execute(sql):
      alreadyhavegame = True

    if alreadyhavegame:
      return render_template("message.html", message="game already in progress")

    creategame = "INSERT INTO game VALUES(NULL, \"" + session['username'] + "\", \"" + opponent + "\", \"RNSQKSNRUUUUUUUU00000000000000000000000000000000uuuuuuuurnsqksnr\", 1, 0, 0);"

    db.cursor().execute(creategame)
    db.commit()

    id = ''
    for row in db.cursor().execute(sql):
	  id = str(row[0])
    return redirect("/game/" + id, code=302)

@app.route("/game/<gameid>")
def game(gameid):
  isplayer1 = False
  isplayer2 = False
  user = ''

  sql = "SELECT * FROM game WHERE gameid = " + gameid + ";"

  player1 = ''
  player2 = ''
  board = ''
  current = 0
  completed = 0
  isFinished = False

  db = get_db()

  for row in db.cursor().execute(sql):
    player1 = str(row[1])
    player2 = str(row[2])
    board = str(row[3])
    current = row[4]
    completed = row[5]
	
  if completed == 1:
    isFinished = True

  if 'username' in session:
    if session['username'] == player1:
      isplayer1 = True
    elif session['username'] == player2:
      isplayer2 = True
    user = session['username']

  return render_template("game.html", username=user ,finished=isFinished, user=user, gameid=gameid, isplayer1=isplayer1, isplayer2=isplayer2, player1=player1, player2=player2, board=board, turn=current)

@app.route("/updategame/<gameid>", methods=['POST'])
def updategame(gameid):
  sql = "SELECT * FROM game WHERE gameid = " + gameid + ";"
  db = get_db()

  board = ''
  for row in db.cursor().execute(sql):
    board = str(row[3])
    player1 = str(row[1])
    player2 = str(row[2])
    current = row[4]

  userShouldBe = ''

  if current == 1:
    userShouldBe = player1
  else:
    userShouldBe = player2

  if session['username'] != userShouldBe:
    return "INVALID: is not users turn"

  fromLocation = request.form['fromLocation'].split()
  toLocation = request.form['toLocation'].split()

  fromLocation[0] = charToNum(fromLocation[0])
  toLocation[0] = charToNum(toLocation[0])

  print fromLocation[0] +"," + fromLocation[1]
  print toLocation[0] + "," + toLocation[1]

  index1 = (7 - (int(fromLocation[1]) - 1))*8 + (int(fromLocation[0]) - 1)

  piece = board[index1]

  if piece == 'u':
    piece = 'p'
  elif piece == 'U':
    piece = 'P'

  board = board[:index1] + "0" + board[index1 + 1:]

  index2 = (7 - (int(toLocation[1]) - 1))*8 + (int(toLocation[0]) - 1)

  board = board[:index2] + piece + board[index2 + 1:]

  newCurrent = ''

  if current == 1:
    newCurrent = 2
  else:
    newCurrent = 1

  sqlupdate = "UPDATE game SET boardstate = \"" + board + "\", current = " + str(newCurrent) + " WHERE gameid = " + gameid + ";"

  print sqlupdate

  db.cursor().execute(sqlupdate)
  db.commit()

  return "updated"

@app.route("/getgame/<gameid>", methods=['POST'])
def getgame(gameid):

  sql = "SELECT * FROM game WHERE gameid = " + gameid + ";"
  db = get_db()

  current = 0
  player1 = ''
  player2 = ''
  currentPlayer = ''
  user = ''

  for row in db.cursor().execute(sql):
    player1 = str(row[1])
    player2 = str(row[2])
    current = row[4]

  if current == 1:
    currentPlayer = player1
  else:
    currentPlayer = player2

  if 'username' in session:
    user = session['username']
    if session['username'] == currentPlayer:
      return "True"

  if user != player1 or user != player2:
    print request.form['current']
    if not currentPlayer == request.form['current']:
      return "True"

  return "False"

@app.route("/completegame/<gameid>", methods=['POST'])
def completegame(gameid):

  db = get_db()
  winner = ''
  
  if request.form['player'] == "pwhite":
    winner = '2'
  else:
    winner = '1'

  sql = "UPDATE game SET completed = 1, winner = " + winner + " WHERE gameid = " + gameid + ";"
  
  db.cursor().execute(sql)
  db.commit()
  
  return ""

def get_image_info(data):
  dimensions = []
  if is_png(data):
    w, h = struct.unpack('>LL', data[16:24])
    width = int(w)
    height = int(h)
    dimensions.append(width)
    dimensions.append(height)
  else:
    raise Exception('not a png image')
  return dimensions


def is_png(data):
  return (data[:8] == '\211PNG\r\n\032\n'and (data[12:16] == 'IHDR'))

@app.route("/image/<player>/<piece>")
def image(player, piece):
  start = '<img src="'
  url = url_for('static', filename='resources/chessimages/'+player+'/'+piece+'.png')
  print os.getcwd() + url
  with open(os.getcwd() + url, 'rb') as f:
        data = f.read()
  width = get_image_info(data)[0]
  height = get_image_info(data)[1]
  end = '"><p>' + str(width) + ',' + str(height) + '</p>'
  return start+url+end, 200
  
@app.route("/copy/<user>")
def copy(user):
  url = url_for('static', filename='resources/chessimages/blank.png')
  url = os.getcwd() + url
  url2 = url_for('static', filename='resources/userimages/'+user+'.png')
  url2 = os.getcwd() + url2
  copyfile(url, url2)
  return ""
  
@app.route("/uploadimage/", methods=['GET','POST'])
def uploadimage():
  if 'username' in session:
    if request.method == 'POST':
      f = request.files['datafile']
      f.save('static/resources/tmpuserimages/' + session['username'] + '.png')

      with open('static/resources/tmpuserimages/' + session['username'] + '.png', 'rb') as f:
        data = f.read()

      try:
        width = get_image_info(data)[0]
        height = get_image_info(data)[1]
        if width > 100 or height > 100:
          os.remove('static/resources/tmpuserimages/' + session['username'] + '.png')
          return render_template("uploadpicture.html", message="File too large!")
        else:
          copyfile('static/resources/tmpuserimages/' + session['username'] + '.png', 'static/resources/userimages/' + session['username'] + '.png')
          os.remove('static/resources/tmpuserimages/' + session['username'] + '.png')

        return render_template("uploadpicture.html", username=session['username'], message="Profile picture uploaded!")
      except:
        return render_template("uploadpicture.html", username=session['username'], message="File is not .png")

    else:
      return render_template("uploadpicture.html", username=session['username'])
  else:
    return render_template("message.html", username="", message="Must be logged in to upload a profile picture")

@app.errorhandler(404)
def page_not_found(error):
  if 'username' in session:
    return render_template("message.html", username=session['username'], message="404 Page not found")
  else:
    return render_template("message.html", username="", message="404 Page not found")

if __name__ == "__main__":
  init(app)
  app.run(host=app.config['ip_address'],
  port=int(app.config['port']),debug=app.config['Debug'], threaded=True)
