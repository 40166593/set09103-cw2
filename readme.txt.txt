1. This application requires bcrypt to install run the following commands
$sudo su
$easy_install py_bcrypt-0.4-py2.7-linux-i686.egg
$exit

2. To reinitalise the database, run $python init_db.py
3. To run the web application, run $python main.py